# Содержание

1. Знакомство с ECManager
 * [Что такое ECManager и зачем он нужен](1-basics/1-1-chto-takoe-ecm.md)
 * [Как зайти в калькулятор](1-basics/1-3-enter-ecm.md)
 * [Интерфейс ECManager](1-basics/1-2-interface-ecm.md)
 * [Важно! Требования к браузеру](1-basics/1-4-browser.md)
2. [Как быстро расчитать смету и сделать коммерческое предложение – краткий пример](2-quick/2-quickcount.md)
3. Как устроен калькулятор
 * [Из каких частей состоит калькулятор и их назначение](3-inside/3-1-parts.md)
 * Элементы
 	* [Что такое элемент](3-inside/3-2-1-element.md)
 	* [Что внутри элемента](3-inside/3-2-2-element.md)
 	* [Как добавить элемент](3-inside/3-2-3-element.md)
 	* [Как редактировать элемент](3-inside/3-2-4-element.md)
 * Шаблоны
 	* [Что такое шаблон](3-inside/3-3-1-template.md)
    * [Что внутри шаблона](3-inside/3-3-2-template.md)
    * [Как добавить шаблон](3-inside/3-3-3-template.md)
    * [Как редактировать шаблон](3-inside/3-3-4-template.md)
 * Бланки коммерческих предложений
    * [Что такое шаблон КП](3-inside/3-4-1-kp.md)
    * [Что внутри шаблона](3-inside/3-4-2-kp.md)
    * [Как добавить шаблон](3-inside/3-4-3-kp.md)
    * [Как редактировать шаблон](3-inside/3-4-4-kp.md)
4.	Расчеты – подробно
 * [Как сделать расчет](4-count/4-1-count.md)
 * [Как добавлять изображения к расчету](4-count/4-2-count.md)
 * [Как редактировать расчет](4-count/4-3-count.md)
 * [Как сделать расчет нестандартной конструкции](4-count/4-4-count.md)
5. Библиотека проектов
 * [Как выбрать проект из библиотеки проектов](5-prj_bibl/5-1-bibl.md)
 * [Как добавить проекты в библтиотеку](5-prj_bibl/5-2-bibl.md)
6. [Техническая поддержка](6-support/6-1-support.md)
