# Требование к браузеру на компьютере

## Это важно.

Главное назначение ECmanager - быстро сосчитать смету и сгенерировать PDF и страницу с коммерческим предложением.
Но разные браузеры по разному обрабатывают PDF-файлы.
Самый беспроблемный (по нашему мнению) - Google Chrome. 

Мы рекомендуем его для работы с ECManager.
